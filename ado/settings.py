"""
Project settings file for ado

This is the bootstrap settings file. settings below are configurable using
environment variables. Most settings changes should not be made here but
instead in one of the proper settings files in the "config" directory.

config directory files:

    config/defaults.py:      base settings
    config/dev.py:           settings overrides for development
    config/production.py     settings overrides for production/deployment
    config/tests.py          settings overrides for test runner
    config/local.py          Not to be checked into version control, use for
                             private settings (e.g. passwords, SECRET_KEY, etc)

"""
import os
import environ
import warnings
from django.core.exceptions import ImproperlyConfigured


# read environment vars from .env file
root = environ.Path(__file__) - 1
env_path = root('../.env')
if os.path.exists(root(env_path)):
    environ.Env.read_env(env_path)

# Set env variable DJANGO_ENV to either 'dev' or 'production' (default: 'dev')
# export DJANGO_ENV=production
ENVIRONMENT = environ.Env()('DJANGO_ENV', default='dev')

# Import either dev or production settings
if ENVIRONMENT == 'dev':
    from .config.dev import *
else:
    from .config.production import *

# import extra settings from local.py
try:
    if ENVIRONMENT != 'test':
        from .config.local import *
except ImportError:
    pass

# Default environment setting types
env = environ.Env(
    DEBUG=(bool, DEBUG),
    INTERNAL_IPS=(list, locals().get('INTERNAL_IPS', tuple())),
    AWS_ACCESS_KEY_ID=(str, locals().get('AWS_ACCESS_KEY_ID', '')),
    AWS_SECRET_ACCESS_KEY=(str, locals().get('AWS_SECRET_ACCESS_KEY', '')),
    EMAIL_HOST=(str, locals().get('EMAIL_HOST', None)),
    EMAIL_HOST_USER=(str, locals().get('EMAIL_HOST_USER', None)),
    EMAIL_HOST_PASSWORD=(str, locals().get('EMAIL_HOST_PASSWORD', None)),
    EMAIL_PORT=(str, locals().get('EMAIL_PORT', None)),
    EMAIL_USE_TLS=(str, locals().get('EMAIL_USE_TLS', False)),
)

# deubg
DEBUG = env("DEBUG", default=ENVIRONMENT == 'dev')

# secret key
try:
    SECRET_KEY = env('SECRET_KEY')
except ImproperlyConfigured:
    SECRET_KEY = locals().get('SECRET_KEY', None)
    if SECRET_KEY is None:
        SECRET_KEY = '__SECRET_KEY__'
        if ENVIRONMENT != 'dev':
            warnings.warn('The SECRET_KEY variable is not set')

# Databases
try:
    DATABASES = {
        'default': env.db()
    }
except ImproperlyConfigured:
    if locals().get('DATABASES') is None:
        raise

# Caches
try:
    default_cache = env.cache()
    if ('default' in locals().get('CACHES', {}) and
            CACHES['default']['BACKEND'] == default_cache['BACKEND']):
        CACHES['default'].update(default_cache)
    else:
        CACHES = {'default': default_cache}

except ImproperlyConfigured:
    if locals().get('CACHES') is None:
        raise


INTERNAL_IPS = env('INTERNAL_IPS')

# AWS credientials
AWS_ACCESS_KEY_ID = env('AWS_ACCESS_KEY_ID')
AWS_SECRET_ACCESS_KEY = env('AWS_SECRET_ACCESS_KEY')

# Email settings
EMAIL_HOST = env('EMAIL_HOST')
EMAIL_HOST_USER = env('EMAIL_HOST_USER')
EMAIL_HOST_PASSWORD = env('EMAIL_HOST_PASSWORD')
EMAIL_PORT = env('EMAIL_PORT')
EMAIL_USE_TLS = env('EMAIL_USE_TLS')
