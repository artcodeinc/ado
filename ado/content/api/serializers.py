from rest_framework import serializers
from ado.media.api.serializers import ImageSerializer, VideoSerializer


class ContentItemSerializer(serializers.ModelSerializer):
    image = ImageSerializer()
    video = VideoSerializer()

    class Meta:
        fields = [
            'id',
            'type',
            'text',
            'image',
            'video',
        ]

    def get_type(self, obj):
        return obj.type
