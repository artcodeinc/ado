import { jQuery as $ } from '~/lib/jQuery'

/**
 * Provides any extensions or fixes to Django changeform
 */
const init = () => {
  // Fix checkboxes not rendering correctly for grouped multiline form rows
  // Django 4.2 - missing "checkbox-row"
  const containers = Array.from(document.querySelectorAll('.flex-container.fieldBox'))
  containers.forEach(container => {
    const checkboxChildren = container.querySelectorAll('input[type="checkbox"], label.vCheckboxLabel')
    if (checkboxChildren.length === 2) {
      container.classList.add('checkbox-row')
    }
  })

  // Give a class for flex-container when using raw_id_fields. We do want these to be laid our horizontally
  const rawIdInputs = [...document.querySelectorAll('input.vForeignKeyRawIdAdminField')]
  rawIdInputs.forEach(input => {
    const isMediaFk = input.parentNode.classList.contains('ImageForeignKeyWidget') || input.parentNode.classList.contains('VideoForeignKeyWidget')
    if (!isMediaFk) {
      const flexContainer = $(input).closest('.flex-container')[0]
      if (flexContainer) {
        flexContainer.classList.add('fk-raw-id-container')
      }
    }
  })
}

export const ChangeFormExtensions = {
  init,
}

export default ChangeFormExtensions
