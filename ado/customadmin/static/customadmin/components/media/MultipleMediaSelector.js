/* eslint no-underscore-dangle: off */

/**
 * Show related media popup
 */
export const showRelatedMediaWindow = (url, name) => {
  return window.open(url, name, 'height=500,width=800,resizable=yes,scrollbars=yes')
}


/**
 * MultipleMediaSelector component
 * Shows a popup to media library change list and will return list of selected
 * mediaitem ids.
 *
 * MultipleMediaSelector().then(selectedIds => { ... do stuff })
 *
 */
export const MultipleMediaSelector = (app = 'media', model = 'mediaitem') => {
  let mediaModel = model.replace(/relation$/, '')
  if (mediaModel === 'media') {
    mediaModel = 'mediaitem'
  }

  return new Promise((resolve, reject) => {
    // Click handler for add link
    const url = `/admin/${app}/${mediaModel}/?_to_field=id&_popup=1`
    const win = showRelatedMediaWindow(url, 'select_mediaitems')

    // Handle django's 'dismissAddRelatedObjectPopup'. This will be called by
    // django when adding an object through the django add_form.
    window.dismissAddRelatedObjectPopup = (_win, newId, newRepr) => {
      window.dismissPopup([newId])
    }

    const _dismissAddAnotherPopup = window.dismissAddAnotherPopup
    window.dismissAddAnotherPopup = window.dismissAddRelatedObjectPopup

    // Handle popup dismiss with selected mediaitem ids
    window.dismissPopup = (selectedIds) => {
      win.close()
      window.dismissAddAnotherPopup = _dismissAddAnotherPopup
      resolve(selectedIds) // resolve promise
    }
  })
}


export default MultipleMediaSelector
