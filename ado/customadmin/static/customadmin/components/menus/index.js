import MenuEditorApp from './MenuEditorApp'


const rootEl = document.getElementById('menu-editor-app')
if (rootEl) {
  MenuEditorApp.init(rootEl)
}
