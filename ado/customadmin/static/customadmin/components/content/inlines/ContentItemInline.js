import toTitleCase from 'titlecase'
import domReady from '~/lib/domReady'
import { jQuery as $ } from '~/lib/jQuery'
import MultipleMediaSelector from '~/components/media/MultipleMediaSelector'
import { addMediaForeignKeyWidgets } from '~/components/media/widgets/MediaForeignKeyWidgets'

import './ContentItemInline.scss'

/**
 * Custom functionality for editing ContentItemInline
 *
 * Each ContentItemInline type has inlines for editing either an Image, Video
 * or a Text section. This sets up functionality allowing to add either an image
 * or text and will hide the other fields in the inline form depending on the
 * type the user selected.
 */
export const ContentItemInline = (inlineGroup) => {
  inlineGroup.classList.add('content-item-inline')

  // Set up inline styles & labels
  const existingRows = [...inlineGroup.querySelectorAll('.inline-related:not(.empty-form)')]
  existingRows.forEach(inline => {
    const typeClass = inline.dataset.contentItemType

    // Add class type
    inline.classList.add(`inline-${typeClass}`)

    // Change inline label text
    const label = inline.querySelector('h3 .inline_label')
    if (label) label.innerText = typeClass

    // Change 'delete' label to remove'
    const deleteLabel = inline.querySelector('.delete .vCheckboxLabel')
    if (deleteLabel) deleteLabel.innerText = 'remove'

    // Show the editable field for this content type
    inline.querySelectorAll('.form-row').forEach(el => {
      if (el.classList.contains(`field-${typeClass}`)) {
        el.style.display = 'block'
      }
    })
  })

  // Find add-row
  const addRow = inlineGroup.querySelector('.add-row')
  if (!addRow) return // no add-row link was found (likely means no add permisison)
  const origAddLink = addRow.querySelector('a')

  // Find editable form fields
  const emptyForm = inlineGroup.querySelector('.inline-related.empty-form')
  if (!emptyForm) return

  /**
   * Initialize new inline when added
   */
  const onInlineAdded = ({ fieldName, label }) => {
    // trigger click event on the original add link to add a new inline row
    $(origAddLink).trigger('click')

    // Add the label to the inline field
    const selector = '.inline-related.last-related:not(.empty-form)'
    const inline = Array.from(inlineGroup.querySelectorAll(selector)).slice(-1)[0]

    // Style the inline and show the editable field for this content type
    inline.classList.add(`inline-${fieldName}`)
    inline.querySelectorAll('.form-row').forEach(el => {
      if (el.classList.contains(`field-${fieldName}`)) {
        el.style.display = 'block'
      }
    })
    const inlineLabel = inline.querySelector('.inline_label')
    inlineLabel.innerText = label

    // prevent initializing ckeditor for non-text inlines
    if (fieldName !== 'text') {
      const textarea = inline.querySelector('.field-text textarea')
      textarea.dataset.processed = '1'
    }

    return inline
  }

  /**
   * Initialize multiple image & video inlines when added
   */
  const onMediaItemsAdded = (selectedIds, { fieldName, label }) => {
    selectedIds.forEach(async (id, i) => {
      const inline = onInlineAdded({ fieldName, label })
      addMediaForeignKeyWidgets()
      const input = $(inline).find(`.field-${fieldName} .vForeignKeyRawIdAdminField`)[0]
      $(input).val(id).trigger('change')
    })
  }

  // Select media
  const selectMedia = async ({ fieldName, label }) => {
    const selectedIds = await MultipleMediaSelector('media', fieldName)
    onMediaItemsAdded(selectedIds, { fieldName, label })
  }

  // Find each editable field type in the inline form
  const formRows = [...emptyForm.querySelectorAll('.form-row')]
  formRows.map(el => ([...el.classList.values()]))
  const fieldTypes = formRows.map(el => {
    return [...el.classList.values()]
      .map(c => /^field-(\w+)$/.exec(c))
      .find(c => !!c)[1]
  }).filter(t => t && t !== 'position')


  fieldTypes.forEach(type => {
    // Create add link for the field type
    const addLink = $(origAddLink).clone(false)[0]
    if (type === 'image') addLink.innerText = 'Add Images'
    else if (type === 'video') addLink.innerText = 'Add Videos'
    else addLink.innerText = `Add ${toTitleCase(type)}`

    addRow.appendChild(addLink)

    // Handle addLink click (different for media vs other fields)
    if (type === 'image' || type === 'video') {
      $(addLink).on('click', (e) => {
        e.preventDefault()
        e.stopPropagation()
        const label = toTitleCase(type)
        selectMedia({ fieldName: type, label })
      })
    }
    else {
      $(addLink).on('click', (e) => {
        e.preventDefault()
        const label = toTitleCase(type)
        onInlineAdded({ fieldName: type, label })
      })
    }
  })

  // Hide the original add link
  origAddLink.style.display = 'none'
}


/**
 * Init for content inline elements
 */
const init = () => {
  domReady(() => {
    setTimeout(() => {
      const inlineGroups = document.querySelectorAll('.content-inline-group')
      if (inlineGroups.length >= 1) inlineGroups.forEach(group => ContentItemInline(group))
      else console.warn('Warning: Could not find any inlines with class: .content-inline-group')
    })
  })
}

export default {
  init,
}
