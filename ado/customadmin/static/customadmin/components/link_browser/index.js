import React from 'react'
import ReactDOM from 'react-dom'
import LinkBrowser from './LinkBrowser'

const id = 'link-browser'
const rootEl = document.getElementById(id)
if (rootEl) {
  ReactDOM.render(
    <LinkBrowser />,
    rootEl,
  )
}
else {
  console.error(`Could not find  "#${id}`)
}
