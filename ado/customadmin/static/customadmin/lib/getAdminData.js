
/**
 * Returns a script tag containing json data for the Django admin. The script
 * tag should have type application/json and be rendered in the Djagno admin.
 *
 * <script id="admin_app_json"
 *         type="application/json">{% admin_app_list_json %}</script>
 */
export const getAdminDataScriptTag = (scriptId = 'admin_app_json') => (
  document.getElementById(scriptId)
)

/**
 * Returns Django admin data from application/json script tag.
 */
export const getAdminData = (scriptId = 'admin_app_json') => {
  const scriptNode = getAdminDataScriptTag(scriptId)
  if (scriptNode) {
    return JSON.parse(scriptNode.innerHTML)
  }
  return {}
}

export default getAdminData
