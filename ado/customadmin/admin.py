import json
from functools import update_wrapper

from django.contrib import admin
from django import http
from django.shortcuts import render
from django.core.exceptions import FieldDoesNotExist
from django.utils.decorators import method_decorator
from django.utils.encoding import force_str
from django.utils.safestring import mark_safe
from django.views.decorators.csrf import csrf_protect
from django.views.decorators.http import require_POST

from taggit.managers import TaggableManager
from ckeditor.widgets import CKEditorWidget

from ado.media import resizer
from ado.media.models import MediaItem, Image, Video, File
from ado.customadmin.utils import get_webpack_bundle_media
from .forms import (
    MediaForeignKeyWidget,
    FileForeignKeyWidget,
    ImageForeignKeyWidget,
    VideoForeignKeyWidget,
    SmartSelect,
    SmartMultiSelect,
    SmartTagSelect,
)


csrf_protect_m = method_decorator(csrf_protect)
require_POST_m = method_decorator(require_POST)


class BaseModelAdminMixin(object):
    """
    A ModelAdmin mixin that provides some customizations
    """

    # Sub-classes can override the following properties to extend functionality.

    # Hide the admin subtitle heading (useful e.g. for singleton models)
    hide_subtitle = False

    # Fields which should use html visual editor
    html_editor_fields = []

    # Fields to use verbose fk media widget
    verbose_media_fk_fields = []

    # Fields to use verbose fk image widget
    verbose_image_fk_fields = []

    # Fields to use verbose fk video widget
    verbose_video_fk_fields = []

    # Fields to use verbose fk file widget
    verbose_file_fk_fields = []

    # Fields to choose for SmartSelect widgets
    smartselect_fields = []

    # Field to use for published/unpublish actions
    publish_model_field = "published"

    ##############################

    def get_actions(self, request):
        actions = super().get_actions(request)
        if self.publish_model_field:
            try:
                self.model._meta.get_field(self.publish_model_field)
                for action in ('make_published_action', 'make_unpublished_action'):
                    actions[action] = self.get_action(action)
            except FieldDoesNotExist:
                pass
        return actions

    def formfield_for_manytomany(self, db_field, request, **kwargs):
        # SmartMultiSelect for m2m fields
        if db_field.name in self.smartselect_fields:
            kwargs['widget'] = SmartMultiSelect
        return super().formfield_for_manytomany(db_field, request, **kwargs)

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        # SmartSelect fields
        if db_field.name in self.smartselect_fields:
            kwargs['widget'] = SmartSelect
        return super().formfield_for_foreignkey(db_field, request, **kwargs)

    # Override custom formfields
    def formfield_for_dbfield(self, db_field, request, **kwargs):
        # html editor fields
        html_editor_fields = {}
        for field in self.html_editor_fields:
            if isinstance(field, str):
                html_editor_fields[field] = 'default'
            else:
                html_editor_fields[field[0]] = field[1]

        if db_field.name in list(html_editor_fields.keys()):
            kwargs['widget'] = CKEditorWidget(
                config_name=html_editor_fields[db_field.name]
            )
            return db_field.formfield(**kwargs)

        # Verbose fk fields for media items
        elif db_field.name in self.verbose_file_fk_fields:
            kwargs['widget'] = FileForeignKeyWidget(
                db_field.remote_field, self.admin_site
            )
            return db_field.formfield(**kwargs)
        elif db_field.name in self.verbose_image_fk_fields:
            kwargs['widget'] = ImageForeignKeyWidget(
                db_field.remote_field, self.admin_site
            )
            return db_field.formfield(**kwargs)
        elif db_field.name in self.verbose_video_fk_fields:
            kwargs['widget'] = VideoForeignKeyWidget(
                db_field.remote_field, self.admin_site
            )
            return db_field.formfield(**kwargs)
        elif db_field.name in self.verbose_media_fk_fields:
            kwargs['widget'] = MediaForeignKeyWidget(
                db_field.remote_field, self.admin_site
            )
            return db_field.formfield(**kwargs)

        elif (
            isinstance(db_field, TaggableManager)
            and db_field.name in self.smartselect_fields
        ):
            kwargs['widget'] = SmartTagSelect

        # Custom media fk fields when using raw_id_fields
        # Same as above, but no need to use custom verbose_*_fk_fields attributes
        elif (
            db_field.name in self.raw_id_fields
            and db_field.name not in self.get_autocomplete_fields(request)
        ):
            if issubclass(db_field.related_model, MediaItem):
                if issubclass(db_field.related_model, Image):
                    kwargs['widget'] = ImageForeignKeyWidget(
                        db_field.remote_field, self.admin_site
                    )
                    return db_field.formfield(**kwargs)
                elif issubclass(db_field.related_model, Video):
                    kwargs['widget'] = VideoForeignKeyWidget(
                        db_field.remote_field, self.admin_site
                    )
                    return db_field.formfield(**kwargs)
                elif issubclass(db_field.related_model, File):
                    kwargs['widget'] = FileForeignKeyWidget(
                        db_field.remote_field, self.admin_site
                    )
                    return db_field.formfield(**kwargs)
                else:
                    kwargs['widget'] = MediaForeignKeyWidget(
                        db_field.remote_field, self.admin_site
                    )
                    return db_field.formfield(**kwargs)

        return super().formfield_for_dbfield(db_field, request, **kwargs)

    # Generate a thumbnail image column
    @admin.display(description='Image')
    def image_column(self, obj):
        if getattr(self, 'get_image'):
            image = self.get_image(obj)
        else:
            raise NotImplementedError(
                "You must implement 'get_image' method on your ModelAdmin "
                "for 'image_column' to work in list_display"
            )

        if image:
            thumb = resizer.get_resized(image, '200x200')
            return mark_safe('<img src="{0}" width="110" />'.format(thumb.url))
        return 'no image'

    # Admin actions
    @admin.action(description="Publish selected %(verbose_name_plural)s")
    def make_published_action(self, request, queryset):
        """
        Admin action to publish selected items
        """
        queryset.update(**{self.publish_model_field: True})
        self.message_user(
            request,
            "Published %s %ss." % (queryset.count(), self.model._meta.model_name),
        )

    @admin.action(description="Unpublish selected %(verbose_name_plural)s")
    def make_unpublished_action(self, request, queryset):
        """
        Admin action to un-publish selected items
        """
        """
        An action to unpublish selected items.
        """
        queryset.update(**{self.publish_model_field: False})
        self.message_user(
            request,
            "Unpublished %s %ss." % (queryset.count(), self.model._meta.model_name),
        )

    # Admin views
    def changelist_view(self, request, extra_context=None):
        if extra_context is None:
            extra_context = {}

        # Alter title to just show the model name
        extra_context.update({'title': self.model._meta.verbose_name_plural.title()})

        return super().changelist_view(request, extra_context)

    def changeform_view(self, request, object_id=None, form_url="", extra_context=None):
        if self.hide_subtitle:
            extra_context = extra_context or {}
            extra_context.update({"subtitle": None})
        return super().changeform_view(request, object_id, form_url, extra_context)


class BaseModelAdmin(BaseModelAdminMixin, admin.ModelAdmin):
    """
    A ModelAdmin class which uses BaseModelAdminMixin automatically
    """

    pass


class SortableAdminListMixin(object):
    """
    An admin mixin which adds a new view to for ordering the position of items.
    """

    # Specify the name of the position field to use (set on subclasses)
    sortable_position_field = 'position'

    @property
    def media(self):
        super_media = super().media
        media = get_webpack_bundle_media(
            'sortablelist', super_media, ['admin/js/jquery.init.js']
        )
        return media

    def get_urls(self):
        """
        Add custom url for sortable list page
        """
        from django.urls import path

        urls = super().get_urls()

        def wrap(view):
            def wrapper(*args, **kwargs):
                return self.admin_site.admin_view(view)(*args, **kwargs)

            wrapper.model_admin = self
            return update_wrapper(wrapper, view)

        info = self.model._meta.app_label, self.model._meta.model_name

        custom_urls = [
            path(
                'organize/',
                wrap(self.organize_view),
                name='%s_%s_organize' % info,
            ),
            path(
                'organize/update/',
                wrap(self.organize_update_position_view),
                name='%s_%s_organize_update_position' % info,
            ),
        ]

        return custom_urls + urls

    def get_sortable_objects(self, request):
        return list(self.get_queryset(request))

    def get_sortable_templates(self, app_label, model_name):
        return [
            'admin/%s/%s/sortable_admin_list.html' % (app_label, model_name),
            'admin/%s/sortable_admin_list.html' % app_label,
            'admin/sortable_admin_list.html',
        ]

    def organize_view(self, request, extra_context=None):
        """
        Organize admin view used to re-position items with drag & drop
        """
        self._check_position_field()

        opts = self.model._meta
        app_label = opts.app_label
        queryset = self.get_sortable_objects(request)
        title = 'Re-order {0}'.format(opts.verbose_name_plural)

        # See if we have an image field to show
        has_sortable_image = hasattr(self, 'get_sortable_image') or hasattr(
            self, 'get_image'
        )
        objects = []
        for obj in queryset:
            if has_sortable_image:
                if hasattr(self, 'get_sortable_image'):
                    image = self.get_sortable_image(obj)
                elif hasattr(self, 'get_image'):
                    image = self.get_image(obj)
            else:
                image = None
            objects.append((obj, image))

        context = {
            'opts': opts,
            'title': title,
            'app_label': app_label,
            'objects': objects,
            'media': self.media,
            'modeladmin': self,
            'has_sortable_image': has_sortable_image,
            'has_add_permission': self.has_add_permission(request),
            'has_change_permission': self.has_change_permission(request),
            'has_view_permission': self.has_view_permission(request),
        }
        context.update(extra_context or {})

        template_names = self.get_sortable_templates(app_label, opts.model_name)

        return render(request, template_names, context)

    @require_POST_m
    @csrf_protect_m
    def organize_update_position_view(self, request):
        """
        Ajax POST view for updating positions
        """
        self._check_position_field()

        request_data = json.loads(request.body.decode('UTF-8'))

        try:
            positions = request_data['positions']
        except KeyError:
            return http.HttpResponseBadRequest('No positions provided')

        try:
            group_name = request_data['group_name']
            group_value = request_data['group_value']
        except KeyError:
            group_name = None
            group_value = None

        try:
            object_ids = [int(i) for i in positions]
        except (ValueError, TypeError):
            return http.HttpResponseBadRequest('invalid object ids')

        for i, object_id in enumerate(object_ids):
            update_dict = {self.sortable_position_field: i}
            if group_name and group_value:
                update_dict.update({group_name: group_value})
            self.model.objects.filter(pk=object_id).update(**update_dict)

        return http.HttpResponse()

    def _check_position_field(self):
        # Make sure sortable_position_field exists on the model class
        try:
            self.model._meta.get_field(self.sortable_position_field)
        except FieldDoesNotExist as e:
            raise FieldDoesNotExist(
                '{0}. Set `sortable_position_field` '
                'on your ModelAdmin class'.format(e.message)
            )


class MultiListFilter(admin.SimpleListFilter):
    """
    A ListFilter that allows selecting multiple values at once.
    """

    template = 'customadmin/multi_filter.html'

    def __init__(self, request, params, model, model_admin):
        super().__init__(request, params, model, model_admin)

        parameter_name = self.parameter_name
        if parameter_name in self.used_parameters:
            self.used_parameters[parameter_name] = self.used_parameters[
                parameter_name
            ].split(',')
        else:
            self.used_parameters[parameter_name] = []

    def choices(self, changelist):
        yield {
            'selected': len(self.value()) == 0,
            'query_string': changelist.get_query_string({}, [self.parameter_name]),
            'display': 'All',
        }
        for lookup, title in self.lookup_choices:
            yield {
                'selected': force_str(lookup) in self.value(),
                'query_string': changelist.get_query_string(
                    {self.parameter_name: lookup}, []
                ),
                'display': title,
            }


class DateListFilter(admin.DateFieldListFilter):
    def __init__(self, model, *args, **kwargs):
        super().__init__(model, *args, **kwargs)
        model_class = model.model
        years = model_class.objects.dates(model.name, 'year', order='DESC')
        self.links += tuple(
            (
                d.year,
                {
                    self.lookup_kwarg_since: str(d),
                    self.lookup_kwarg_until: str(
                        d.replace(year=d.year + 1, month=1, day=1)
                    ),
                },
            )
            for d in years
        )


# Import customizations to Django's auth admin
from . import auth_admin  # noqa
