import re
from django import forms
from webpack_loader import utils as webpack

# We should not load these patterns
COMMONS_CHUNKS = re.compile(r'customadmin\.(runtime|commons)(\.[a-f0-9]+)?\.js')


def get_webpack_bundle_media(bundle_name, media, dependencies=[]):
    # Get webpack assets
    webpack_js = [
        f
        for f in webpack.get_files(bundle_name, 'js', config='CUSTOMADMIN')
        if not COMMONS_CHUNKS.search(f['name'])
    ]
    webpack_css = webpack.get_files(bundle_name, 'css', config='CUSTOMADMIN')

    bundle_media = media + (
        forms.Media(
            js=dependencies + [f['url'] for f in webpack_js],
            css={**media._css, **{'all': [f['url'] for f in webpack_css]}},
        )
    )

    return bundle_media
