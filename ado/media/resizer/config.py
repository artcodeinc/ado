from django.conf import settings

# The module path to the resizer class to use
DEFAULT_IMAGE_RESIZER = getattr(
    settings,
    'DEFAULT_IMAGE_RESIZER',
    'ado.media.resizer.SorlImageResizer',
)

# Whether to use a placeholder fallback image when the source file is not
# found. This could be a performance hit for remote storages, so leaving it off
# is recommended for production settings.
IMAGE_RESIZER_USE_DUMMY = getattr(
    settings,
    'IMAGE_RESIZER_USE_DUMMY',
    False,
)
