"""
Signals listeners for ado.media
"""
from django.db.models import signals
from django.dispatch import receiver
from django.conf import settings
from django_cleanup.signals import cleanup_pre_delete

from ado.media.models import Image, Video
from ado.media.util import get_image_dimensions
from ado.media import resizer


DEFAULT_IMAGE_SIZES = (
    ('thumb', '200x200'),
    ('largethumb', '360x300'),
)

IMAGE_SIZES = getattr(settings, 'SCALED_IMAGE_SIZES', DEFAULT_IMAGE_SIZES)


@receiver(signals.pre_save, sender=Image)
def add_image_dimensions(instance, **kwargs):
    """Calculate dimensions for Image instances"""
    if not kwargs.get('raw') and instance.filename:
        if not instance.width or not instance.height:
            width, height = get_image_dimensions(instance.filename)
            instance.width = width
            instance.height = height


@receiver(signals.pre_save, sender=Video)
def add_video_image_dimensions(instance, **kwargs):
    """Calculate dimensions for Image instances"""
    if not kwargs.get('raw') and instance.video_image:
        if not instance.video_image_width or not instance.video_image_height:
            width, height = get_image_dimensions(instance.video_image)
            instance.video_image_width = width
            instance.video_image_height = height


@receiver(signals.post_save, sender=Image)
def generate_image_thumbnails(instance, created, **kwargs):
    """
    Pre-generate resized images on save
    """
    if not kwargs.get('raw'):
        for key, size in IMAGE_SIZES:
            resizer.get_resized(instance.filename, size)


@receiver(signals.post_save, sender=Video)
def generate_video_thumbnails(instance, **kwargs):
    """
    Pre-generate resized images for Video.video_image on save
    """
    if not kwargs.get('raw'):
        if instance.video_image:
            for key, size in IMAGE_SIZES:
                resizer.get_resized(instance.video_image, size)


@receiver(cleanup_pre_delete)
def cleanup_thumbnails(**kwargs):
    """
    Clean up thumbnail files when ImageField model is deleted or changed.
    """
    file_ = kwargs.get('file')
    resizer.delete_resized(file_)
