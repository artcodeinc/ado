# Generated by Django 4.2.6 on 2023-10-08 02:29

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('media', '0016_copy_jsonfield_data'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='mediaupload',
            name='metadata_migrate',
        ),
        migrations.RemoveField(
            model_name='video',
            name='data_migrate',
        ),
    ]
