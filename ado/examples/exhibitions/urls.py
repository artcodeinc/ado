from django.urls import path
from ado.examples.exhibitions import views
from ado import menus

urlpatterns = [
    path('', views.index, name='exhibitions-exhibition-index'),
    path('current/', views.current, name='exhibitions-exhibition-current'),
    path('past/', views.past, name='exhibitions-exhibition-past'),
    path('upcoming/', views.future, name='exhibitions-exhibition-future'),
    path('view/<str:slug>/', views.view, name='exhibitions-exhibition-view'),
]

menus.register_url('Exhibitions', name='exhibitions-exhibition-index')
