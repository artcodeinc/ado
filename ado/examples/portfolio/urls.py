from django.urls import path
from ado.examples.portfolio import views
from ado import menus

urlpatterns = [
    path('', views.index, name='portfolio-collection-index'),
    path('<str:slug>', views.view, name='portfolio-collection-view'),
]

menus.register_url('Portfolio', name='portfolio-collection-index')
