from django.urls import path
from ado.examples.pages import views

urlpatterns = [
    path('<str:slug>/', views.view, name='pages-page-view'),
]
