from django.urls import path, re_path

from ado.examples.blog import feeds
from ado.examples.blog import views
from ado import menus


urlpatterns = [
    path('', views.blog_index, name='blog-index'),

    # Main URLS
    re_path(r'^(?P<year>\d{4})/(?P<month>\w{3})/$', views.month_archive, name='blog-month-archive'),
    re_path(r'^(?P<year>\d{4})/(?P<month>\w{3})/(?P<day>\d{1,2})/$', views.day_archive, name='blog-day-archive'),
    re_path(r'^(?P<year>\d{4})/(?P<month>\w{3})/(?P<day>\d{1,2})/(?P<slug>[\w-]+)/$', views.entry_detail, name='blog-entry-view'),
    re_path(r'^(?P<year>\d{4})/(?P<month>\w{3})/(?P<slug>[\w-]+)/$', views.entry_month_detail),

    # Alternative URLS with number format for month
    re_path(r'^(?P<year>\d{4})/(?P<month>\d{2})/$', views.month_archive_alt),
    re_path(r'^(?P<year>\d{4})/(?P<month>\d{2})/(?P<day>\d{1,2})/$', views.day_archive_alt),
    re_path(r'^(?P<year>\d{4})/(?P<month>\d{2})/(?P<day>\d{1,2})/(?P<slug>[\w-]+)/$', views.entry_detail_alt),
    re_path(r'^(?P<year>\d{4})/(?P<month>\d{2})/(?P<slug>[\w-]+)/$', views.entry_month_detail_alt),

    # Feeds
    path('feed/', feeds.LatestEntriesFeed(), name='blog-feed'),
    path('feed/rss/', feeds.LatestEntriesFeed(), name='blog-feed-rss'),
    path('feed/atom/', feeds.AtomLatestEntriesFeed(), name='blog-feed-atom'),
]

menus.register_url('Blog', name='blog-index')
