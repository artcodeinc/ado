from django.db import models
from django.apps import apps
from django.core.cache import cache
from polymorphic.models import PolymorphicModel


class SettingField:
    """
    Base setting field
    """

    defaults = {}

    def __init__(self, *args, **kwargs):
        mergedKwargs = {}
        mergedKwargs.update(self.defaults)
        mergedKwargs.update(kwargs)
        super().__init__(*args, **mergedKwargs)


class CharSetting(SettingField, models.CharField):
    """
    Setting field for character data
    """

    defaults = {
        'max_length': 512,
        'blank': True,
    }


class TextSetting(SettingField, models.TextField):
    """
    Setting field for text data
    """

    defaults = {
        'blank': True,
    }


class ConfigSettingGroup(PolymorphicModel):
    """Base configuration model"""

    class Meta:
        app_label = 'configuration'


CACHE_KEY = 'configuration_context_processors'


def get_config_settings():
    """
    Return a data structure of all configuration settings.
    """

    # Get configuration app
    try:
        config_app = apps.get_app_config('configuration')
    except LookupError:
        # If configuration app is not installed, return an empty object
        return {}

    # First check cache
    config_settings = cache.get(CACHE_KEY)
    if config_settings is not None:
        return config_settings

    # Get config group for each model
    model_classes = [m for m in config_app.get_models() if m is not ConfigSettingGroup]
    groups = []
    for cls in model_classes:
        instance, created = cls.objects.get_or_create()
        groups.append(instance)

    # Build config settings data structure
    config_settings = {}

    for group in groups:
        settings = {}
        ignore_fields = (
            'id',
            'configsettinggroup_ptr',
            'polymorphic_ctype',
        )
        fields = [f for f in group._meta.fields if f.name not in ignore_fields]

        for field in fields:
            settings[field.name] = getattr(group, field.name)
        config_settings[group._meta.model_name] = settings

    # Cache the results
    cache.set(CACHE_KEY, config_settings, timeout=None)

    return config_settings
