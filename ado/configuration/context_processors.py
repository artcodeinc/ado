from ado.configuration.models import get_config_settings


def config_settings(request):
    return {
        'config_settings': get_config_settings(),
    }
