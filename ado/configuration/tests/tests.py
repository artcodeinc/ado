from django.test import TestCase
from ado.configuration.models import get_config_settings
from .models import TestGroup


class SettingsTest(TestCase):
    def test_create_settings(self):
        group, created = TestGroup.objects.get_or_create(title='Test')
        self.assertTrue(created)
        self.assertTrue(group.id)
        self.assertEqual(group.title, 'Test')

    def test_get_config_settings(self):
        group, _ = TestGroup.objects.get_or_create(
            title='Test',
            description='testing',
            keywords='testing, test',
        )
        settings = get_config_settings()
        self.assertTrue('testgroup' in settings)
        settings = settings['testgroup']
        self.assertEqual('Test', settings['title'])
        self.assertEqual('testing', settings['description'])
        self.assertEqual('testing, test', settings['keywords'])
